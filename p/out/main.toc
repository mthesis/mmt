\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Top Tagging}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Neuronal Networks}{7}{0}{1}
\beamer@subsectionintoc {1}{3}{Autoencoder}{11}{0}{1}
\beamer@subsectionintoc {1}{4}{Graphs}{16}{0}{1}
\beamer@subsectionintoc {1}{5}{Graph Autoencoder}{21}{0}{1}
\beamer@sectionintoc {2}{Problems}{26}{0}{2}
\beamer@subsectionintoc {2}{1}{For few Particles}{26}{0}{2}
\beamer@subsectionintoc {2}{2}{Scaling}{28}{0}{2}
\beamer@subsectionintoc {2}{3}{Why does the Graph fall of?}{33}{0}{2}
\beamer@subsectionintoc {2}{4}{Why is the trivial Model so good?}{43}{0}{2}
\beamer@subsectionintoc {2}{5}{Whats Next}{56}{0}{2}
\beamer@sectionintoc {3}{Backup}{57}{1}{3}
\beamer@subsectionintoc {3}{1}{Does it actually learn something?}{57}{1}{3}
\beamer@subsectionintoc {3}{2}{auc by loss}{59}{1}{3}
\beamer@subsectionintoc {3}{3}{Graphs over Dense}{63}{1}{3}
\beamer@subsectionintoc {3}{4}{Potential of a working pt combination}{65}{1}{3}
