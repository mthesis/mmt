import numpy as np
import matplotlib.pyplot as plt
import itertools

np.random.seed(2)

m=0.2
b=0.5
s=0.005
s2=0.01
n=100

x=np.arange(0,1,1/n)
y=m*x+b

r=np.random.normal(0,s,x.shape)
r2=np.random.normal(0,s2,x.shape)
r=list(itertools.accumulate(r))
y+=r
y+=r2

fit=np.polyfit(x,y,1)


np.savez_compressed("data",x=x,y=y,m=[m],b=[b],s=[s],s2=[s2],r=r,r2=r2)

plt.plot(x,y,"o")
plt.plot(x,fit[0]*x+fit[1],alpha=0.5)
#plt.axis("off")
plt.show()










