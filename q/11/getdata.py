import numpy as np

def getdata(f="01"):
  f=np.load("../"+f+"/data.npz")
  return f["x"],f["y"]
def getsignal():
  return getdata("09")


def getnn():
  f=np.load("../04/evalb.npz")
  return f["x"],f["p"][:,0]



def go1d(q):

  #print(q)
  #exit()

  x,y=[],[]
  for aq in q:
    x.append(aq[0])
    y.append(aq[1])
  return np.array(x),np.array(y)

def getae():
  f=np.load("../05/evalb.npz")
  return go1d(f["p"])
def getsignalae():
  f=np.load("../05/signalb.npz")
  return go1d(f["p"])






