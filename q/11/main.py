import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys

from getdata import *
from cauc import caucd


pm=True#production mode

if len(sys.argv)>1:
  pm=False

#sns.set(style="darkgrid")

def save(fnam):
  if not pm:return
  plt.savefig("imgs/"+fnam+".png",format="png")
  plt.savefig("imgs/"+fnam+".pdf",format="pdf")

def dualprint(a,b):
  for i in range(len(a)):
    print(a[i],b[i])

#if pm:plt.figure(figsize=(15,12))

x,y=getdata()
sx,sy=getsignal()
ax,ay=getae()
sax,say=getsignalae()


#dualprint(sx,sax)

#exit()



def delta(x,y,ax,ay):
  r=np.sqrt((x-ax)**2+(y-ay)**2)
  return r

d=delta(x,y,ax,ay)
sd=delta(sx,sy,sax,say)

maxd=np.max([np.max(d),np.max(sd)])
mind=np.min([np.min(d),np.min(sd)])
rang=[mind,maxd]

bc=10

#plt.hist(d,range=rang,bins=bc,alpha=0.5,color="blue",label="background")
#plt.hist(sd,range=rang,bins=bc,alpha=0.5,color="orange",label="signal")
#plt.show()
#exit()

cd=np.concatenate((d,sd),axis=0)
cy=np.concatenate((np.zeros_like(d),np.ones_like(sd)),axis=0)

q=caucd(d=cd,y=cy)

tpr=q["tpr"]
fpr=q["fpr"]

auc=q["auc"]
e30=q["e30"]

print("auc",auc,"e30",e30)


plt.plot(tpr,1/(fpr+0.001))
plt.yscale("log",nonposy="clip")
plt.xlim([0,1])
#plt.ylim([0,1])
plt.show()



exit()



x,y=getdata()
sns.scatterplot(x,y,marker="D",label="background")
sx,sy=getsignal()
sns.scatterplot(sx,sy,marker="D",label="signal")



#plt.xlim([0,1])
#plt.ylim([0.2,0.7])

#plt.tick_params(
#    axis='both',          # changes apply to both axis
#    which='both',      # both major and minor ticks are affected
#    bottom=False,      # ticks along the bottom edge are off
#    top=False,         # ticks along the top edge are off
#    left=False,
#    right=False,
#    labelbottom=False,
#    labelleft=False) # labels along the bottom edge are off
#plt.xlabel("x")
#plt.ylabel("y")


#plt.legend()



ax,ay=getae()
sns.lineplot(x=ax,y=ay,label="auto encoder")
#sns.scatterplot(ad)


plt.show()



