import numpy as np
import csv
import sys

from advload import *
from createmodel import *



def makelenx(q,x):
  q=str(q)
  while len(q)<x:
    q="0"+q
  return q


def a2load(j="b",**kwargs):
  if j=="a":j="val_acc"
  if j=="b":j="val_loss"

  with open("history.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        q[i].append(float(row[i]))


  row=[]

  for i in range(1,len(q)):
    if qn[i]==j:
      row=q[i]

  if j=="val_acc":
    ii=np.argmax(row)
  else:
    ii=np.argmin(row)
  ii+=1

  
  return aload("models/weights"+makelenx(ii,4)+".tf") 
  

















