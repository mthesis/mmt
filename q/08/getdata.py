import numpy as np

def getdata():
  f=np.load("../06/data.npz")
  return f["x"],f["y"]


def getnn():
  f=np.load("../04/evalb.npz")
  return f["x"],f["p"][:,0]



def go1d(q):

  #print(q)
  #exit()

  x,y=[],[]
  for aq in q:
    x.append(aq[0])
    y.append(aq[1])
  return x,y

def getae():
  f=np.load("../07/evalb.npz")
  return go1d(f["p"])






