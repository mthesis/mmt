import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys

from getdata import *


pm=True#production mode

if len(sys.argv)>1:
  pm=False

sns.set(style="darkgrid")

def save(fnam):
  if not pm:return
  plt.savefig("imgs/"+fnam+".png",format="png")
  plt.savefig("imgs/"+fnam+".pdf",format="pdf")


x,y=getdata()
if pm:plt.figure(figsize=(15,12))





sns.scatterplot(x,y,marker="D",label="data")


plt.xlim([-1,1])
plt.ylim([-1,1])

plt.tick_params(
    axis='both',          # changes apply to both axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False) # labels along the bottom edge are off
plt.xlabel("some feature")
plt.ylabel("another feature")


plt.legend()


save("without_fit")



ax,ay=getae()
sns.lineplot(x=ax,y=ay,label="auto encoder")
#sns.scatterplot(ad)

save("auto_encoder")

plt.show()



