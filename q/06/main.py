import numpy as np
import matplotlib.pyplot as plt
import itertools

np.random.seed(3)

m=0.55
b=0.2
s=0.02
n=100

R=0.9


t=np.arange(0,1,1/n)
x=R*np.cos(2*np.pi*t)
y=R*np.sin(2*np.pi*t)

rx=np.random.normal(0,s,x.shape)
ry=np.random.normal(0,s,y.shape)

x+=rx
y+=ry




np.savez_compressed("data",x=x,y=y,t=t,r=[R],rx=rx,ry=ry)

plt.plot(x,y,"o")
#plt.plot(x,fit[0]*x+fit[1],alpha=0.5)
#plt.axis("off")
plt.show()










