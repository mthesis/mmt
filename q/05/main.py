from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
import sys

from ageta import *
from getdata import *




shallwrite=True



if len(sys.argv)>1:
  shallwrite=bool(int(sys.argv[1]))

if shallwrite:
  for i in range(10):
    print("##################################################################")
  print("!!WRITING!!")
  for i in range(10):
    print("##################################################################")



def twoshuffle(x,y):
  s=np.random.randint(100000)
  np.random.seed(s)
  np.random.shuffle(x)
  np.random.seed(s)
  np.random.shuffle(y)
  return x,y

ax,ay=getdata()
data=np.transpose(np.array([ax,ay]))
np.random.shuffle(data)
x=data[:80]
vx=data[80:]




vae,encoder,decoder=getvaeq()
models=(encoder,decoder)



#input_shape=vae.input_shape
#print("input_shape",input_shape)

#output_shape=vae.compute_output_shape(input_shape)
#print("output_shape",output_shape)

#exit()



cb=[#keras.callbacks.EarlyStopping(monitor='val_loss',patience=patience),
                   keras.callbacks.TerminateOnNaN()]
#if shallwrite:
  #al=LossHistory()
  #cb.append(al)
  
  #cb.append(BatchStore(N=100))
if shallwrite:
  cb.append(keras.callbacks.ModelCheckpoint("modelb.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
  cb.append(keras.callbacks.ModelCheckpoint("models/weights{epoch:04d}.tf",verbose=0,save_best_only=False,period=1,save_weights_only=True))
  cb.append(keras.callbacks.CSVLogger("history.csv"))
else:
  cb.append(keras.callbacks.CSVLogger("history_anyway.csv"))


# train the autoencoder
vae.fit(x,
        epochs=epochs,
        batch_size=batch_size,
        #)
        #validation_split=0.1)
        validation_data=(vx, None),
        verbose=verbose,
        callbacks=cb)




if shallwrite:vae.save_weights('vae.tf')

#printananan(vae)




