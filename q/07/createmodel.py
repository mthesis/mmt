import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization,Conv2D
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model


def denseladder(c,n=3,truestart=False):
  if type(c)==list:
    ret=[]
    for ac in c:
      ret.append(denseladder(ac,n=n,truestart=truestart))
    return ret

  if truestart:n-=1

  #generates a list of Dense sizes going from 1 to c in n steps (excluding 1 and c)
  ret=[]
  if truestart:ret.append(1)
  fact=1.0
  mul=c**(1/(n+1))
  for i in range(n):
    fact*=mul
    ret.append(fact)
  return ret

def handlereturn(inn1,raw,com,inn2,decom,shallvae):
  #inn1  :   initial input Variable
  #raw   :   preconverted input Variable, for comparison sake
  #com   :   compressed Variable
  #inn2  :   input for decoder
  #decom :   decompressed decoder Variable
  #shallvae: shall i thread this like a variational auto encoder? hier just a bodge of an solution




  
  if not int(raw.shape[-1])==int(decom.shape[-1]):
    print("returning failed at comparison shape comparison(1) of",raw.shape,"and",decom.shape)
    assert False
  if not int(com.shape[-1])==int(inn2.shape[-1]):
    print("returning failed at compression shape comparison of",com.shape,"and",inn2.shape)
    assert False

  
  c1=com
  c2=com
  
  if shallvae:
    c1=Dense(int(c1.shape[-1]))(c1)
    c2=Dense(int(c2.shape[-1]))(c2)
    for i in range(100):print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("!!!running variational!!!")
    for i in range(100):print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
  return inn1,raw,c1,c2,[],inn2,decom



def createbothmodels(gs,out,n,shallvae=True,**kwargs):

  activation="sigmoid"




  inp1=Input(shape=(2,))
  #g=sortparam(g,m)

  raw=inp1
  q=inp1

  #g=norm(g)

  q=Dense(4,activation=activation)(q)
  q=Dense(8,activation=activation)(q)
  q=Dense(4,activation=activation)(q)
  q=Dense(2,activation=activation)(q)
  q=Dense(1,activation=activation)(q)

  inp2=Input(shape=(1,))
  p=inp2

  p=Dense(2,activation=activation)(p)
  p=Dense(4,activation=activation)(p)
  p=Dense(8,activation=activation)(p)
  p=Dense(4,activation=activation)(p)
  p=Dense(2,activation=activation)(p)




  return handlereturn(inp1,raw,q,inp2,p,shallvae)












