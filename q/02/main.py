import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns
import sys

from getdata import *


pm=True#production mode

if len(sys.argv)>1:
  pm=False

#sns.set(style="darkgrid")

def save(fnam):
  plt.legend()
  if not pm:return
  plt.savefig("imgs/"+fnam+".png",format="png")
  plt.savefig("imgs/"+fnam+".pdf",format="pdf")


x,y=getdata()
if pm:plt.figure(figsize=(6,9))





plt.plot(x,y,"o",marker="D",label="data")


plt.xlim([0,1])
plt.ylim([0.2,0.7])

plt.tick_params(
    axis='both',          # changes apply to both axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False) # labels along the bottom edge are off
plt.xlabel("x")
plt.ylabel("y")


plt.legend()


save("without_fit")


lfit=np.polyfit(x,y,1)
fy=lfit[0]*x+lfit[1]

plt.plot(x,fy,label="linear",linewidth=5.0,alpha=0.8)

save("linear")

nx,ny=getnn()

plt.plot(nx,ny,label="Network",linewidth=5.0,alpha=0.8)

save("neuronal_network")

ax,ay=getae()
plt.plot(ax,ay,label="auto encoder",linewidth=5.0,alpha=0.8)
#sns.scatterplot(ad)

save("auto_encoder")

plt.show()



